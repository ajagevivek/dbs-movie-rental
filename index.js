// Following Function is responsible for Navigating User to Movie Details page with Movie ID
function redirectToMovieDetail(movieId){
  window.location.href = `/movierental/movie-details/?movie-id=${movieId}`;
}

// Following Function is responsible for Getting Movies Based on user's input string
function getMovieByKeyword(keyword) {

  // Checking if Input String/Keyword is Not empty
  if (!keyword || !keyword.trim()) {

    // for Learning purpose https://api.jquery.com/html/
    $(`#searchResults`).html('');

     // for Learning purpose https://api.jquery.com/css/
    $(`#searchResults`).css('display', 'none');;
    return;
  }
  
  // Defining API endpoint For Search API
  const url = `${API_END_POINT}/search/movie?api_key=${API_KEY}&language=en-US&query=${keyword}&page=1&include_adult=false`;
  
  // Defining Callback Function which will be executed on getting successful API response from Server
  const callback = function (response) {

    // Checking if data is in expected format so that system won't crash in case BACKEND is down
    if (response && response.results) {
      var innerHTMLforSearchBlock = "";
      
      // Looping through API search Results and generating HTML blocks to insert in the DOM
      // for Learning purpose https://api.jquery.com/each/
      $.each(response.results, function (index, value) {

        // Appending to the Single Search Template

        innerHTMLforSearchBlock =
        innerHTMLforSearchBlock +
          `<div onclick="redirectToMovieDetail(${value.id});" class="single-result">
          <img src=${
            value.poster_path
              ? `https://image.tmdb.org/t/p/w220_and_h330_face${value.poster_path}`
              : `./images/place_holder_movie.png`
          } />
          <div class="column-containers"> 
          <span class="single-result-heading">
              ${value.title}
          </span>
          <span class="single-result-subheading">
          ${value.overview}
          </span>
          </div>
      </div>`;
      });

      // Pushing Data inside DOM
      $(`#searchResults`).html(innerHTMLforSearchBlock);

      // Display Search Results
      $(`#searchResults`).css('display', 'block');;
    }
  };

  // Calling API with Above Configuration
  callMovieDBApi(url, callback, "get");
}

// Following Function is Generic Function responsible for Generating Callbacks for Listing Data.
// Based on various Predefined Types it will Return Different Function 
// Function is useful if we want to added more Listing like Specific Genre

function generateCallBackFunction(type) {
  
  return function (response) {
    // Checking for Successful API 
    if (response && response.results) {

      var innerHTMLblock = "";
      // Looping through Results
      $.each(response.results, function (index, value) {
        
        innerHTMLblock =
        innerHTMLblock +
          `<div  class="movie-single-item">
            <div  onclick="redirectToMovieDetail(${value.id});" class="movie-item-poster">
                <div class="overlay-info">
                    <span>
                        ${value.overview}
                    </span>
                </div>
                <img src=${
                  value.poster_path
                    ? `https://image.tmdb.org/t/p/w220_and_h330_face${value.poster_path}`
                    : `./images/place_holder_movie.png`
                } />
            </div>
            <span class="movie-heading"> ${value.title}</span>
            <span class="movie-genre">Rating ${value.vote_average}</span>
        </div>`;
      });

      // Added intentional Delay to show Skeleton Loader
      setTimeout(() => {

        $(`#${type}`).html(innerHTMLblock);

        // We are using https://kenwheeler.github.io/slick/ slick library to handle Sliders 
        // Slider is configured to handle Multiple Device Sizes, We are using generic Breakpoints
        // based on whether Device is 1. HD screen Laptop 2. SD screen Laptop 3. Tablet 4. Mobile Phone

        $(`#${type}`).slick({
          slidesToShow: 7,
          slidesToScroll: 3,
          autoplay: true,
          prevArrow: false,
          nextArrow: false,
          infinite: true,
          responsive: [
            {
              breakpoint: 1600,
              settings: {
                slidesToShow: 6,
                slidesToScroll: 3,
               
              }
            },
            {
              breakpoint: 1025,
              settings: {
                slidesToShow: 5,
                slidesToScroll: 3,
               
              }
            },
            {
              breakpoint: 800,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 520,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
          ]
        });
      }, 2000);
    }
  };
}

// Following Function responsible for getting URL for Listing API for further API documentation
// refer https://developers.themoviedb.org/3/movies
// API_END_POINT is defined in separate utils.js for better manging folder structure and increase
// Reusability and Easy Configuration 

function getUrlBasedOnType(type) {
  switch (type) {
    case "popularMovies": {
      return `${API_END_POINT}/movie/popular?api_key=${API_KEY}`;
    }
    case "recommendedMovies": {
      return `${API_END_POINT}/movie/630656/similar?api_key=${API_KEY}&language=en-US&page=1`;
    }
    case "nowPlaying": {
      return `${API_END_POINT}/movie/now_playing?api_key=${API_KEY}&language=en-US&page=1`;
    }
    case "topRated": {
      return `${API_END_POINT}/movie/top_rated?api_key=${API_KEY}&language=en-US&page=1`;
    }
    default: {
      return ``;
    }
  }
}


// Following Function responsible for getting dashboard listing based of Type provided

function getDashboardData(type) {
 
  // Turning On Skeleton Loader 
  showSkeleton(type)
 
  // Getting URL for Calling API
  const url = getUrlBasedOnType(type);

  // Generating Callback Function mapping API response with DOM elements
  const callback = generateCallBackFunction(type);

  // Finally Calling API with Above Configuration
  callMovieDBApi(url, callback, `GET`);
}


// Checking When DOM is ready and We can call API to fetch data and replace DOM elements with Live Data
// for learning purpose refer : https://learn.jquery.com/using-jquery-core/document-ready/
$(document).ready(function () {
  
  // following function is to prevent Executing this File on other Routes since there are few function 
  // in this file which are imported on other Routes as well

  if(window.location.pathname!='/movierental/'){
    console.log('Not Executing Orignal Script')
    return
  }
  
  // Getting Popular Movies Data and Pushing them inside DOM
  getDashboardData(`popularMovies`);

  // Getting Recommended Movies Data and Pushing them inside DOM
  getDashboardData(`recommendedMovies`);
  
  // Getting Recently Updated Movies Data and Pushing them inside DOM
  getDashboardData(`nowPlaying`);

  // Getting Top rated Movies Data and Pushing them inside DOM
  getDashboardData(`topRated`);

  // Following Function is Hacky way to handle Landscape mode on Mobile Devices
  // We are parsing User Agent to identify Whether Device is mobile or not
  if (/mobile/i.test(navigator.userAgent) && !/ipad|tablet/i.test(navigator.userAgent)) {
    $("#warningHandler").removeClass('control-warning')
  }

  // Following Function binds Keyup event on Input Element to Fire Search API
  $("#searchBoxInput").keyup(function (event) {
    
    // Handling for Different event type and deprecations across browser versions
    // Refer https://stackoverflow.com/questions/4471582/keycode-vs-which
    var keycode = event.keyCode ? event.keyCode : event.which;
    var valid =
      (keycode > 47 && keycode < 58) || // number keys
      (keycode > 64 && keycode < 91) || // letter keys
      (keycode > 95 && keycode < 112);

    // Refer Checking if either Enter / Backspace / Alpha-Numeric Character is pressed to fire API
    if (keycode == "13" || keycode == "8" || valid) {
      getMovieByKeyword($("#searchBoxInput").val());
    }

    // Stoppin Default Event Bubble 
    event.stopPropagation();
  });
  return;
});
