const API_KEY = `0e7e81ffb857b97c9f578599c6c2e897`;
const API_END_POINT = `https://api.themoviedb.org/3`;

// var localCache = {
//   /**
//    * timeout for cache in millis
//    * @type {number}
//    */
//   timeout: 30000,
//   /** 
//    * @type {{_: number, data: {}}}
//    **/
//   data: {},
//   remove: function (url) {
//       delete localCache.data[url];
//   },
//   exist: function (url) {
//     console.log(localCache.data[url])
//       return !!localCache.data[url] && ((new Date().getTime() - localCache.data[url]._) < localCache.timeout);
//   },
//   get: function (url) {
//       console.log('Getting in cache for url' + url);
//       return localCache.data[url].data;
//   },
//   set: function (url, cachedData, callback) {
//       localCache.remove(url);
//       localCache.data[url] = {
//           _: new Date().getTime(),
//           data: cachedData
//       };
//       if ($.isFunction(callback)) callback(cachedData);
//   }
// };

// $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
//   console.log('herererer')
//   if (options.cache) {
//     console.log( originalOptions)
//       var complete = originalOptions.complete || $.noop,
//           url = originalOptions.url;
//       //remove jQuery cache as we have our own localCache
//       options.cache = false;
//       options.beforeSend = function () {
//         console.log(localCache)
//         console.log(url)
//         if (localCache.get(url)) {
//             console.log('Cache exists')
//               complete(localCache.get(url));
//               return false;
//           }
//           return true;
//       };
//       options.complete = function (data, textStatus) {
//         console.log('hrtr dretting into vavhe')
//         console.log(url)
//         localCache.set(url, data, complete);
//       };
//   }
// });

function redirectToRoute(route){
  console.log(route);
  window.location.href = route;
}

function switchToDarkMode(){
  if (document.body.classList.contains('darkMode')) {
    // Turning the theme off:
    document.body.classList.remove('darkMode');
    // Reverse logic on the button text, so that users can turn
    // the theme back on:
  } else {
    document.body.classList.add('darkMode');
  }
}


function showSkeleton(type){
  console.log(type)
  $(`#${type}`).scheletrone({
  });
}
function callMovieDBApi(url, callback, method, data) {
    const payload = {
      async: true,
      crossDomain: true,
      //set this option for caching API calls so page speed will be optimized
      // cache:true,
      url: url,
      method,
      data,
      headers: {},
    };
  
    $.ajax(payload).done(callback);
  }