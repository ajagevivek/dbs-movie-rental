// Following Function is responsible for getting Movie ID from Query Params
var movieId = function (name) {
  var results = new RegExp("[?&]" + name + "=([^&#]*)").exec(
    window.location.search
  );
  return results !== null ? results[1] || 0 : false;
};

// Following Function is responsible for redirecting to Order Review
function redirectToOrderPage(movieId){
  window.location.href = `/movierental/order-review?movie-id=${movieId}`;
}


$(document).ready(function () {

  // Setting up Slider for Gallery 
  // refer https://kenwheeler.github.io/slick/

  $("#gallery-slider").slick({
    lazyLoad: "ondemand",
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    prevArrow: false,
    nextArrow: false,
    responsive: [
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 520,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
    ]
  });

  
  // Turning on SKELETON loading for Website Blocks
  showSkeleton("movieInfo");
  showSkeleton("majorInfoContainer");
  showSkeleton("castListingID");

  // Checking movie-id is there in QUERY params if not redirecting to listing page

  if (movieId("movie-id")) {
    
    // Setting callback for Movie-Details API

    var callback = function (response) {
      console.log(response);
      if (response && response.id) {
        var newGenre = "";
        
        // Looping through Various Generes and Making Tag Blocks
        $.each(response.genres, function (index, value) {
          newGenre =
            newGenre +
            `<div class="tag">
                <span>
                  ${value.name}
                </span>
             </div>`;
        });

         // Generating Movie Details Block from Response
        const newData = ` <div id="movieInfo" class="movie-info">
                <span class="movie-title">
                    ${response.title}
                </span>
                <span class="movie-subtitle">
                ${response.tagline}
                </span>
                <div class="tags">
                    ${newGenre}
                </div>
                <div class="movie-extra-info">
                    <div class="extra-details-movie">
                        <img src="../images/duration.png" alt="duration" />
                        <span> ${response.runtime} Mins </span>
                    </div>
                    <div class="extra-details-movie">
                        <img src="../images/white_star.png" alt="duration" />
                        <span>${response.vote_average} </span>
                    </div>
                    <div class="extra-details-movie">
                        <img src="../images/calender_icon.png" alt="duration" />
                        <span> ${response.release_date} </span>
                    </div>
                    <div class="extra-details-movie">
                        <img src="../images/globe.png" alt="duration" />
                        <span> ${response.status} Globally </span>
                    </div>
                </div>
            </div>`;

        // Pushing Generated HTML block to DOM
        $(`#movieInfo`).html(newData);

        // To handle Skeleton Loading
        $(`.tag`).addClass("add-blue-background");

        // Updating Poster Image
        $(`.movie-bkg`).css("background-image",`url(${`https://image.tmdb.org/t/p/w220_and_h330_face${response.backdrop_path}`})`);
       
         // Getting Movie ID which will be passed to redirectToOrderPage function
        let movieIdurl = movieId("movie-id");
        
        const leftDetailsCont = `<div id="majorInfoContainer" class="detail-movie-container column-containers">
        <span class="category-label">
            Overview
        </span>
        <div id="tagsContainer" class="tag-container">
           ${newGenre}
        </div>
        <span class='desc-movie'>
        ${response.overview}
        </span>
        <div id='reviewContainers'  class="ratings-container column-container">
                </div>
        <div onclick="redirectToOrderPage(${movieIdurl})" class="big-red-button">
            <img src="https://i.imgur.com/PEtg1jF.png" />
            <span>
                $500 Rent Now
            </span>
        </div>
    </div>`;

        // Pushing Overview Block to the DOM

        $(`#majorInfoContainer`).html(leftDetailsCont);
      }
    };

    // Calling Movie Details API

    callMovieDBApi(
      `${API_END_POINT}/movie/${movieId(
        "movie-id"
      )}?api_key=${API_KEY}&language=en-US`,
      callback,
      "get"
    );

     // Setting Callback for cast information
    var castCallback = function (response) {
      console.log(response);
      if (response && response.id) {
        var newCast = "";
        $.each(response.cast, function (index, value) {
          newCast =
            newCast +
            `<div class="case-info">
            <img src=${
              value.profile_path
                ? `https://image.tmdb.org/t/p/w220_and_h330_face${value.profile_path}`
                : `../images/place_holder_movie.png`
            } />
            <span>
                ${value.name}
            </span>
        </div>`;
        });

         // Pushing CAST information to DOM
        $(`#castListingID`).html(newCast);
      }
    };

   
    // Calling CAST details API
    callMovieDBApi(
      `${API_END_POINT}/movie/${movieId(
        "movie-id"
      )}/credits?api_key=${API_KEY}&language=en-US`,
      castCallback,
      "get"
    );

    // Calling PREVIEW movie API to get Movie Trailor Link

    var previewApiCallback = function (response) {
      if (response && response.id) {
        var ytId = "";
        $.each(response.results, function (index, value) {
          if (value.site == "YouTube") {
            ytId = value.key;
          }
        });
        var newYT = ``;
        if (ytId) {
          newYT = `<div class="preview-container">
                <div class="video-container">
                    <iframe src="https://www.youtube.com/embed/${ytId}" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
            </div>`;
        }

        // Pushing Trailor Block to the DOM

        $(`#trailorVideo`).html(newYT);
      }
    };

    // Calling PREVIEW API 
    callMovieDBApi(
      `${API_END_POINT}/movie/${movieId(
        "movie-id"
      )}/videos?api_key=${API_KEY}&language=en-US`,
      previewApiCallback,
      "get"
    );

    
    // Getting data for Related Movies API

    var relatedMoviesApi = function (response) {
      console.log(response);
      if (response && response.results) {
        var relatedMoviesDiv = ``;

        $.each(response.results, function (index, value) {
          relatedMoviesDiv =
            relatedMoviesDiv +
            `<div onclick="redirectToMovieDetail(${value.id});" class="movie-single-item">
            <div class="movie-item-poster">
                <div class="overlay-info">
                    <span>
                        ${value.overview}
                    </span>
                </div>
                <img src=${
                  value.poster_path
                    ? `https://image.tmdb.org/t/p/w220_and_h330_face${value.poster_path}`
                    : `../images/place_holder_movie.png`
                } />
            </div>
            <span class="movie-heading"> ${value.title}</span>
            <span class="movie-genre">Rating ${value.vote_average}</span>
        </div>`;
        });

        // PUSH data to the DOM
        $(`#relatedMoviesDiv`).html(relatedMoviesDiv);

         // Once data is pushed turning ON the slider
        $("#relatedMoviesDiv").slick({
            lazyLoad: "ondemand",
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: true,
            prevArrow: false,
            nextArrow: false,
            responsive: [
              {
                breakpoint: 1025,
                settings: {
                  slidesToShow: 4,
                  slidesToScroll: 3,
                 
                }
              },
              {
                breakpoint: 800,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 2
                }
              },
              {
                breakpoint: 520,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2
                }
              },
            ]
          });
      }
    };
    
    //Calling API for recommendation based on Above Config

    callMovieDBApi(
        `${API_END_POINT}/movie/${movieId(
          "movie-id"
        )}/recommendations?api_key=${API_KEY}&language=en-US`,
        relatedMoviesApi,
        "get"
      );


      //Following Function is proposed but not Implemented
      var reviewsCallback = function (response) {
        console.log(response);
        if (response && response.results) {
          var relatedMoviesDiv = ``;
  
          $.each(response.results, function (index, value) {
            relatedMoviesDiv =
              relatedMoviesDiv +
              `<div class="rating">
              <img src=${value && value.author_details && value.author_details.avatar_path && value.author_details.avatar_path.substring(1)} />
              <div class="review-content column-container">
                  <span class="review-heading">
                      ${value.author_details.username}
                  </span>
                  <span class="review-subtitle">
                  ${value.content}
                  </span>
              </div>
          </div>`;
          });
  
          $(`#reviewContainers`).html(relatedMoviesDiv);
          
        }
      };
  
      // callMovieDBApi(
      //     `${API_END_POINT}/movie/${movieId(
      //       "movie-id"
      //     )}/reviews?api_key=${API_KEY}&language=en-US`,
      //     reviewsCallback,
      //     "get"
      //   );


  } else {
    window.location.href = "/movierental";
  }
});
