$(document).ready(function () {

    $( "#startdatepicker" ).datepicker();
    $( "#enddatepicker" ).datepicker();
})

function submitOrderInfo(){

   
        const fullName =  $( "#fullName" ).val()
        const phoneNumber =  $( "#phoneNumber" ).val()
        const startdatepicker =  $( "#startdatepicker" ).val()
        const enddatepicker =  $( "#enddatepicker" ).val()
        
        if(fullName && phoneNumber && startdatepicker &&enddatepicker ){
            sweetAlert("Successfully Place Order", `Details Submitted :\n`+'Name '+fullName+'\n PhoneNumber '+phoneNumber+'\n Start Date '+startdatepicker+'\n End Date '+enddatepicker, "success");

        }else{
            sweetAlert("Failed to Place Order", 'Please Check Details You Have Entered', "error");

        }

        return;
}